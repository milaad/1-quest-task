// eslint-disable-next-line no-unused-vars

export default ($axios) => ({
  getCities() {
    return $axios.get('/cities')
  },
  itineraries() {
    return $axios.get('/itineraries')
  },
  itinerary(slug) {
    return $axios.get('/itinerary/' + slug)
  },
})
