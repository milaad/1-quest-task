import Vue from 'vue'
import 'pretty-checkbox/dist/pretty-checkbox.css'
import PrettyCheckbox from 'pretty-checkbox-vue'

Vue.use(PrettyCheckbox)
