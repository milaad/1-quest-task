// eslint-disable-next-line no-unused-vars
import repository from '~/api'

export default function ({ $axios, redirect, app, store }, inject) {
  // Create a custom axios instance
  const axiosCustom = $axios.create({
    headers: {
      // common: {
      //   'Content-Type': 'application/json',
      //   Accept: 'application/json, text/plain, */*',
      //   'X-Requested-With': 'XMLHttpRequest',
      // },
    },
  })

  axiosCustom.setBaseURL(
    process.env.API_URL || 'http://localhost:8010/proxy/api/plan/v1'
  )
  const api = repository(axiosCustom)

  inject('api', api)
}
